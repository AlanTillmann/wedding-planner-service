import cors from 'cors';
import express from 'express';
import { MissingResourceError } from './error';
import { Wedding } from './entities';
import { Expense } from './entities';
import WeddingService from '../services/wedding-service';
import ExpenseService from '../services/expense-service';
import { WeddingServiceImpl } from '../services/wedding-service-impl';
import { ExpenseServiceImpl } from '../services/expense-service-impl';


const app = express();
app.use(express.json()); // Middleware
app.use(cors());

const weddingService:WeddingService = new WeddingServiceImpl();
const expenseService:ExpenseService = new ExpenseServiceImpl();

/*
Endpoint: GET all weddings
Description: Get all weddings within our current wedding table: returning the ID, date, name, location, and budget of the wedding.
*/
app.get("/weddings", async (req, res) =>
{
    const wedding:Wedding[] = await weddingService.retrieveAllWeddings();
    res.status(200);
    res.send(wedding);
});

/*
Endpoint: GET wedding by ID
Description: Gets the wedding with matching ID within our current client table: returning the ID, date, name, location, and budget of the wedding.
             Returns an error if the specified wedding ID does not exist.
*/
app.get("/weddings/:id", async (req, res) =>
{
    try
    {
        const weddingId = Number(req.params.id);
        const wedding:Wedding = await weddingService.retrieveWeddingById(weddingId);
        res.status(200);
        res.send(wedding);
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
});

/*
Endpoint: POST a new wedding
Description: Registers a new wedding to our wedding table.  They will be given a default wedding ID and specified date, name, location, and budget.
             Returns an error if more or less than the required field(s) are provided.
*/
app.post("/weddings", async (req, res) =>
{
    if(Object.keys(req.body).length > 4)
        {
            throw new MissingResourceError("message: Cannot add more properties than date, name, location, and budget for the wedding.");
        }
    let wedding:Wedding = req.body;
    wedding = await weddingService.registerWedding(wedding);
    res.status(201);
    res.send(wedding);
});

/*
Endpoint: PUT update wedding by ID
Description: Updates the wedding information in the wedding table. Wedding ID provided must match URL ID.
             Returns an error if there are more than 2 properties or the URL ID does not match the ID of the body.
*/
app.put("/weddings/:id", async (req, res) =>
{
    try
    {
        if(Object.keys(req.body).length > 5)
        {
            throw new MissingResourceError("message: Cannot add properties other than wedding ID, date, name, location, and budget.");
        }
        const id = Number(req.params.id);
        let wedding:Wedding = req.body; // gives you all the info you need
        if (id === req.body.id)
        {
            wedding = await weddingService.modifyWedding(wedding);
            res.status(200);
            res.send(wedding);
        }
        else
        {
            res.status(403);
            res.send({"message": "Error. ID of URL must match body ID."});
        }
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404); 
            res.send(error);
        }
    }
});

/*
Endpoint: DELETE wedding
Description: Deletes a wedding from our wedding table with matching wedding ID. Returns an error if the wedding ID 
             does not exist.
*/
app.delete("/weddings/:id", async (req, res) =>
{
    try
    {
        const id = Number(req.params.id);
        let weddingExists:Boolean = req.body;
        weddingExists = await weddingService.removeWedding(id);
        res.status(205);
        res.send(weddingExists);
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
});

// ACCOUNTS ---------------------------------------------------------------------------------------------------------------------

/*
Endpoint: POST new expense for a wedding
Description: Creates a new expense for an existing wedding.  URL specifies the wedding ID and and the expense ID is auto generated.
             The reason and amount fields must be specified.  Returns an error if more than the reason and amount fields are given, 
             the wedding ID is invalid, or the amount provided is below zero.
*/
app.post("/weddings/:id/expenses", async (req, res) =>
{
    const id:number = Number(req.params.id);
    try
    {
        if(Object.keys(req.body).length > 3)
        {
            res.status(403);
            res.send({"message": "Error. Cannot take in other properties other than reason and amount."});
            return;
        }
        else
        {
            const wedding:Wedding = await weddingService.retrieveWeddingById(id);
        }
    }
    catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return; // We use return as to make sure we do not send multiple responses for our
        }
    }

    let expense:Expense = req.body; // JS Object
    if (Number(expense.amount) > 0)
    {
        expense.wedding_id = id;
        expense = await expenseService.registerExpense(expense);
        res.status(201);
        res.send(expense);
    }
    else
    {
        res.status(403);
        res.send({"message": "Error. Amount must be positive."})
    }
});

/*
Endpoint: GET expenses of a specific wedding
Description: Verifies that the wedding ID is valid, then makes a list with all expenses for every wedding and iterates through them
             choosing the ones that have a matching wedding_id with the URL id.  Returns an error message if the wedding does not have any
             expenses or the wedding ID is not valid.
*/
app.get("/weddings/:id/expenses", async (req, res) =>
{
    const id:number = Number(req.params.id);
    try
    {
        const wedding:Wedding = await weddingService.retrieveWeddingById(id);
    }
    catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }

    const expenses:Expense[] = await expenseService.retrieveAllExpenses();
    const weddingExpenses:Expense[] = [];
    for (let i = 0; i < expenses.length; i++)
    {
        if (expenses[i].wedding_id === id)
        {
            weddingExpenses.push(expenses[i]);
        }
    }
    if (weddingExpenses.length === 0) // If the wedding has no expenses, we return a status 404 instead of an empty array
    {
        res.status(404);
        res.send({"message": `The wedding with wedding_id ${id} has no expenses.`});
        return;
    }
    res.status(200);
    res.send(weddingExpenses);    
});

/*
Endpoint: GET expense with specific ID
Description: Gets all expenses matching the specified URL ID.  Returns an error if the ID does not match
             any existing expenses.
*/
app.get("/expenses/:id", async (req, res) =>
{
    try
    {
        const expenseId = Number(req.params.id);
        const expense:Expense = await expenseService.retrieveExpenseById(expenseId);
        res.status(200);
        res.send(expense);
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
});

/*
Endpoint: PUT updates the expense information (currently only reason and amount)
Description: Must specify the id, the reason, and the new amount of the account along with the associated wedding_id.  
             Returns an error if more fields are given, and if the ID of the URL does not match any existing id values.
*/
app.put("/expenses/:id", async (req, res) =>
{
    try
    {
        if(Object.keys(req.body).length > 5)
        {
            throw new MissingResourceError("message: Cannot add properties other than id, reason, amount, wedding_id, and a photo.");
        }
        const id = Number(req.params.id);
        let expense:Expense = req.body; 
        if (id === req.body.id)
        {
            expense = await expenseService.modifyExpense(expense);
            res.status(200);
            res.send(expense);
            return;
        }
        else
        {
            res.status(403);
            res.send({"message": "Error. ID of URL must match body ID."});
        }
        
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404); 
            res.send(error);
        }
    }
});

/*
Endpoint: DELETE an expense
Description: Deletes a expense with a matching ID of the URL.  Returns an error if the id does not exist.
*/
app.delete("/expenses/:id", async (req, res) =>
{
    try
    {
        const id = Number(req.params.id);
        const expenseExists = await expenseService.removeExpense(id);
        res.status(205);
        res.send(expenseExists);
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
});

app.listen(3001, ()=>
{
    console.log("Application Started");
});