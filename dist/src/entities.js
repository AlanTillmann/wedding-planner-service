"use strict";
exports.__esModule = true;
exports.Expense = exports.Wedding = void 0;
var Wedding = /** @class */ (function () {
    function Wedding(id, date, name, location, budget) {
        this.id = id;
        this.date = date;
        this.name = name;
        this.location = location;
        this.budget = budget;
    }
    ;
    return Wedding;
}());
exports.Wedding = Wedding;
var Expense = /** @class */ (function () {
    function Expense(id, reason, amount, wedding_id, photo) {
        this.id = id;
        this.reason = reason;
        this.amount = amount;
        this.wedding_id = wedding_id;
        this.photo = photo;
    }
    ;
    return Expense;
}());
exports.Expense = Expense;
