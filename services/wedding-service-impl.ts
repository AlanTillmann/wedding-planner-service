// SERVICE LAYER!
// RELIES ON THE DATA LAYER AND PROVIDES "SERVICES" TO THE API LAYER

import { WeddingDAOPostres } from "../src/daos/wedding-dao-postgres-impl";
import { Wedding } from "../src/entities";
import WeddingService from "./wedding-service";

export class WeddingServiceImpl implements WeddingService
{

    weddingDAO:WeddingDAOPostres = new WeddingDAOPostres();

    registerWedding(wedding:Wedding): Promise<Wedding> 
    {
        return this.weddingDAO.createWedding(wedding);
    }

    retrieveAllWeddings(): Promise<Wedding[]> 
    {
        return this.weddingDAO.getAllWeddings();
    }

    retrieveWeddingById(id:number): Promise<Wedding> 
    {
        return this.weddingDAO.getWeddingById(id);
    }

    modifyWedding(wedding: Wedding): Promise<Wedding> 
    {
        return this.weddingDAO.updateWedding(wedding);
    }

    removeWedding(id:number): Promise<Boolean>
    {
        return this.weddingDAO.deleteWeddingById(id);
    }
    
}
