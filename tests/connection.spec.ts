import {client} from '../src/connection'

// client is the main object we will use to interact with our database

test("Should create a connection ", async ()=>
{
    const result = await client.query('select * from wedding');
});

afterAll(async()=>
{
    client.end(); // should close our connection when the test is over
});