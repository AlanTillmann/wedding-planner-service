import { client } from "../src/connection";
import { ExpenseDAO } from "../src/daos/expenses-dao";
import { ExpenseDAOPostres } from "../src/daos/expenses-dao-postgres-impl";
import { Expense } from "../src/entities";

const expensesDAO:ExpenseDAO = new ExpenseDAOPostres();

const testExpenses:Expense = new Expense(1, "Bride's Dress", 1500, 1, "");

test("Create an expense", async ()=>
{
    const result:Expense = await expensesDAO.createExpense(testExpenses);
    expect(result.id).not.toBe(0); 
});

test("Get all expenses", async ()=>
{
    let expense1:Expense = new Expense(1, "Bride's Dress", 1500, 1, "");
    let expense2:Expense = new Expense(1, "Cake", 500, 1, "");
    let expense3:Expense = new Expense(1, "Decorations", 800, 1, "");
    await expensesDAO.createExpense(expense1);
    await expensesDAO.createExpense(expense2);
    await expensesDAO.createExpense(expense3);

    const expenses:Expense[] = await expensesDAO.getAllExpenses();

    expect(expenses.length).toBeGreaterThanOrEqual(3);
});

test("Get expense by Id", async ()=>
{
    let expense:Expense = new Expense(1, "Bride's Dress", 1500, 1, "");
    expense = await expensesDAO.createExpense(expense);
    let retrievedExpense:Expense = await expensesDAO.getExpenseById(expense.id);

    expect(retrievedExpense.id).toBe(expense.id);
});

test("Update expense by Id", async ()=>
{
    let expense:Expense = new Expense(1, "Bride's Dress", 1500, 1, "");
    expense = await expensesDAO.createExpense(expense);

    // to update an object we just edit it and then pass it into a method
    expense.amount = 2000;
    expense = await expensesDAO.updateExpense(expense);

    expect(expense.amount).toBe(2000);
});

test("Delete expense by Id", async ()=>
{
    let expense:Expense = new Expense(1, "Bride's Dress", 1500, 1, "");
    expense = await expensesDAO.createExpense(expense);

    const result:boolean = await expensesDAO.deleteExpenseById(expense.id);
    expect(result).toBeTruthy();
});

afterAll(async()=>
{
    client.end(); // should close our connection when the test is over
});