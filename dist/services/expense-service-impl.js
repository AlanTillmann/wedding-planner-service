"use strict";
exports.__esModule = true;
exports.ExpenseServiceImpl = void 0;
var expenses_dao_postgres_impl_1 = require("../src/daos/expenses-dao-postgres-impl");
var ExpenseServiceImpl = /** @class */ (function () {
    function ExpenseServiceImpl() {
        this.expenseDAO = new expenses_dao_postgres_impl_1.ExpenseDAOPostres();
    }
    ExpenseServiceImpl.prototype.registerExpense = function (expense) {
        return this.expenseDAO.createExpense(expense);
    };
    ExpenseServiceImpl.prototype.retrieveAllExpenses = function () {
        return this.expenseDAO.getAllExpenses();
    };
    ExpenseServiceImpl.prototype.retrieveExpenseById = function (id) {
        return this.expenseDAO.getExpenseById(id);
    };
    ExpenseServiceImpl.prototype.modifyExpense = function (expense) {
        return this.expenseDAO.updateExpense(expense);
    };
    ExpenseServiceImpl.prototype.removeExpense = function (id) {
        return this.expenseDAO.deleteExpenseById(id);
    };
    return ExpenseServiceImpl;
}());
exports.ExpenseServiceImpl = ExpenseServiceImpl;
