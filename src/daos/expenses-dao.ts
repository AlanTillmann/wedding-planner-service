import { Expense } from "../entities";

// A DAO should support the CRUD operations (Create, Read, Update, Delete)
export interface ExpenseDAO
{
    createExpense(expenses:Expense):Promise<Expense>;
    getAllExpenses():Promise<Expense[]>;
    getExpenseById(id:number):Promise<Expense>;
    updateExpense(expenses:Expense):Promise<Expense>;
    deleteExpenseById(id:number):Promise<boolean>;
}