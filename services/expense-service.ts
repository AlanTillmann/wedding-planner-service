import { Expense } from "../src/entities";

export default interface ExpenseService
{
    registerExpense(expense:Expense):Promise<Expense>;
    retrieveAllExpenses():Promise<Expense[]>;
    retrieveExpenseById(id:number):Promise<Expense>;
    modifyExpense(expense:Expense):Promise<Expense>; 
    removeExpense(id:number):Promise<Boolean>;
}